package com.ker.tech.controller;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ker.tech.Model.User;
import com.ker.tech.Repository.UserRepository;

@RestController
@CrossOrigin(origins="*")
public class MyController {
	
	@Autowired
	UserRepository UserRepository;
	//@Autowired
	//private BCryptPasswordEncoder bCryptPasswordEncoder;

	 @GetMapping("/user")
	    public List<User> index(){
	        return UserRepository.findAll();
	    }
	 
	    @GetMapping("/user/{id}")
	    public Optional<User> show(@PathVariable String id){
	        int idemp = Integer.parseInt(id);
	        return UserRepository.findById(idemp);
	    }
	  @PostMapping("/test")
	  public String test(@RequestBody User user)
	  {
		System.out.println(user);
		return "test";
	  }
	  @PostMapping("/save")
	  public User save(@RequestBody User user) throws NoSuchAlgorithmException
	  {
		  
		SecureRandom random= new SecureRandom();
		byte[] salt = new byte[16];
		random.nextBytes(salt);
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		//md.update(salt);
		
		String ss= Base64.getEncoder().encodeToString(salt);
		byte[] hashedPassword = md.digest((ss+user.getPwd()).getBytes(StandardCharsets.UTF_8));
		System.out.println(hashedPassword);
		user.setPwd(Base64.getEncoder().encodeToString(hashedPassword));
		user.setSalt(Base64.getEncoder().encodeToString(salt));
		System.out.println(user);
		return UserRepository.save(user);
	  }	  
	 
	  @PostMapping("/login")
	  
	  
	  @GetMapping("/delete")
	  public void deleteAllUser()
	  {
		UserRepository.deleteAll();
	  }	
	  @GetMapping("/delete/{id}")
	  public void delete(@PathVariable String id)
	  {		 
		  int idemp = Integer.parseInt(id);
		  System.out.println(idemp);
		  UserRepository.deleteById(idemp);
	  }	
	  
}
