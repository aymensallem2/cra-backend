package com.ker.tech.Repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.ker.tech.Model.User;

@Repository
public interface UserRepository extends CrudRepository<User,Integer> {

	List<User> findAll();
	Optional<User> findById(int id);
	User save(User user);
	void deleteAll();
	void deleteById(int id); 
	


}
