package com.ker.tech.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employe")
public class User {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idemp;
	
	@Column(name = "nom")
	private String nom;

	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "pseudo")
	private String pseudo; 
	
	public String getPseudo() {
		return pseudo;
	}

	@Column(name = "pwd")
	private String pwd; 
	
	@Column(name = "salt")
	private String salt; 
	
	@Column(name = "date_embauche")
	private String date_embauche; 
	
	@Column(name = "Contrat")
	private String contrat; 
	
	@Column(name = "email")
	private String email; 
	
	


	public User(int idemp, String nom, String prenom, String pseudo, String pwd, String salt, String date_embauche, String contrat,String email) {
		super();
		this.idemp = idemp;
		this.nom = nom;
		this.prenom = prenom;
		this.pseudo = pseudo;
		this.pwd = pwd;
		this.salt = salt;
		this.date_embauche = date_embauche;
		this.contrat = contrat;
		this.email = email;
		
	}

	public User() {
		super();
	}

	public int getIdemp() {
		return idemp;
	}

	public void setIdemp(int idemp) {
		this.idemp = idemp;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getDate_embauche() {
		return date_embauche;
	}

	public void setDate_embauche(String date_embauche) {
		this.date_embauche = date_embauche;
	}

	public String getContrat() {
		return contrat;
	}

	public void setContrat(String contrat) {
		this.contrat = contrat;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [idemp=" + idemp + ", nom=" + nom + ", prenom=" + prenom + ", pseudo=" + pseudo + ", pwd=" + pwd
				+ ", salt=" + salt + ", date_embauche=" + date_embauche + ", contrat=" + contrat + ", email=" + email
				+ "]";
	}

	
	
}
